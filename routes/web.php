<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('parent',function(){

    return view('cms.parent');
});

Route::get('temp',function(){

    return view('cms.temp');
});

Route::resource('admin', AdminController::class);

Route::fallback(function(){
    echo "Not Found , please try agin";
    });
