@extends('cms.parent')

@section('titel', 'change')


@section('style')

@section('title-page', 'Index change')

@section('samll-title','change')


@section('content')

<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">index change</h3>

          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default">
                  <i class="fas fa-search"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table table-hover table-bordered text-nowrap">
            <thead>
              <tr>
                <th>ID</th>
                <th>first name</th>
                <th>last name</th>
                <th>email</th>
                <th>mobile</th>
                <th>gender</th>
                <th>status</th>
                {{-- <th>Created_at</th>
                <th>updated_at</th> --}}
                <th>Setting</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($change as $change)
                    <tr>
                        <td>{{$change->id}}</td>
                        <td>{{$change->user ? $change->user->firstName : ''}}</td>
                        <td>{{$change->user ? $change->user->lastName : ''}}</td>
                        <td>{{$change->email}}</td>
                        <td>{{$change->user ? $change->user->mobile : ''}}</td>
                        <td>{{$change->user ? $change->user->gender : ''}}</td>
                        <td>{{$change->user ? $change->user->status : ''}}</td>
                        {{-- <td>{{$change->created_at}}</td>
                        <td>{{$change->updated_at}}</td> --}}
                        <td><div class="btn-group">
                            {{-- @can('edit_admin') --}}
                            <a href="{{route('change.edit' , $change->id)}}">
                                <button type="button" class="btn btn-info">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </a>
                            {{-- @endcan --}}

                            {{-- <form action="{{route('change.destroy', $change->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </form>
                            <button type="button" class="btn btn-success">
                              <i class="fas fa-eye"></i>
                            </button> --}}


                            {{-- @can('delete_Admin') --}}
                            <a href="#" onclick="performDestroy({{$change->id}},this)" class="btn btn-danger">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                            {{-- @endcan --}}


                            {{-- <button type="button" class="btn btn-success">
                              <i class="fas fa-eye"></i>
                            </button> --}}
                          </div></td>

                  </tr>
                @endforeach


            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>

@endsection


@section('script')

<script>
    function performDestroy(id , ref){
        confirmDestroy('/cms/admin/change/'+id, ref);
    }
</script>

@endsection
