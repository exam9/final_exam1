@extends('cms.parent')

@section('titel', 'create change')


@section('style')

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">


<link rel="stylesheet" href="{{ asset('cms/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('cms/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

@endsection
@section('title-page', 'create change')

@section('samll-title','change')


@section('content')

 <!-- Main content -->
 <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Quick Example</h3>
            </div>
            <!-- /.card-header -->




            {{-- ajax --}}

            <form id="create_form">
                @Csrf
              {{-- <div class="card-body">


                <div class="row">
                    <div class="col-md-6">


                        <div class="form-group">
                            <label>roles</label>

                            <select class="form-control role" style="width: 100%;" name="role_id" id="role_id">
                                @foreach ($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>



                </div>

              </div> --}}


                  <div class="row">
                <div class="form-group col-md-4">
                  <label for="firstName">firstName</label>
                  <input type="text" class="form-control" name="firstName" id="firstName" placeholder="Enter thie first name">
                </div>

                <div class="form-group col-md-4">
                    <label for="lastName">lastName</label>
                    <input type="text" class="form-control" name="lastName" id="lastName" placeholder="Enter thie lastName">
                  </div>

                  <div class="form-group col-md-4">
                    <label for="mobile">mobile</label>
                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Enter mobile">
                  </div>

                <div class="form-group col-md-4">
                  <label for="email">email</label>
                  <input type="email" class="form-control" name="email" id="email" placeholder="Enter thie email">
                </div>

                <div class="form-group col-md-4">
                    <label for="password">password</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Enter thie password">
                  </div>


                  <div class="form-group col-md-4">
                    <label for="date_Birth">date Of Birth</label>
                    <input type="date" class="form-control" name="date_Birth" id="date_Birth" placeholder="Enter thie date of Birth">
                  </div>


                  <div class="col-md-3">
                    <div class="form-group">
                        <label for="gender">Gender</label>

                        <select class="form-select form-select-sm" name="gender" id="gender" style="width: 100%;"
                         aria-label=".form-select-sm example">


                            <option value="Male">Male</option>
                            <option value="female">feMale</option>

                        </select>
                    </div>
                  </div>



                  <div class="col-md-3">
                    <div class="form-group">
                        <label for="status">status</label>

                        <select class="form-select form-select-sm" name="status" id="status" style="width: 100%;"
                         aria-label=".form-select-sm example">

                            <option value="Active">Active</option>
                            <option value="InActive">InActive</option>

                        </select>
                    </div>
                  </div>











                </div>




              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="button" onclick="preformStore()" class="btn btn-primary">store</button>
              </div>
            </form>
          </div>



        </div>



      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->



@endsection


@section('script')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>


<script src="{{ asset('cms/plugins/select2/js/select2.full.min.js') }}"></script>


<script>

$('.role').select2({
        theme: 'bootstrap4'
    })



flatpickr("#date_Birth", {});


    function preformStore(){
        let data ={
            role_id: document.getElementById('role_id').value,
            firstName: document.getElementById('firstName').value,
            lastName: document.getElementById('lastName').value,
            mobile: document.getElementById('mobile').value,
            email: document.getElementById('email').value,
            password: document.getElementById('password').value,
            date_Birth: document.getElementById('date_Birth').value,
            gender: document.getElementById('gender').value,
            status: document.getElementById('status').value,

        }

        store('/cms/admin/change/', data);
    }
</script>

@endsection
