@extends('cms.parent')

@section('titel', 'edit change')


@section('style')

@section('title-page', 'edit change')

@section('samll-title','edit')


@section('content')

 <!-- Main content -->
 <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Quick Example</h3>
            </div>
            <!-- /.card-header -->



            <form id="update_form">
                @Csrf

              <div class="card-body">




                  <div class="row">
                <div class="form-group col-md-6">
                  <label for="firstName">firstName</label>
                  <input type="text" class="form-control" name="firstName" id="firstName" value="{{$change->user->firstName}}">
                </div>

                <div class="form-group col-md-6">
                    <label for="lastName">lastName</label>
                    <input type="text" class="form-control" name="lastName" id="lastName" value="{{$change->user->lastName}}">
                  </div>

                  <div class="form-group col-md-4">
                    <label for="mobile">mobile</label>
                    <input type="text" class="form-control" name="mobile" id="mobile" value="{{$change->user->mobile}}">
                  </div>

                <div class="form-group col-md-8">
                  <label for="email">email</label>
                  <input type="email" class="form-control" name="email" id="email" value="{{$change->email}}">
                </div>

                {{-- <div class="form-group col-md-6">
                    <label for="password">password</label>
                    <input type="password" class="form-control" name="password" id="password" value="{{$change->password}}">
                  </div> --}}



                  <div class="col-md-3">
                    <div class="form-group">
                        <label for="gender">Gender</label>

                        <select class="form-select form-select-sm" name="gender" id="gender" style="width: 100%;"
                         aria-label=".form-select-sm example">

                            <option selected>{{$change->user->gender}}</option>
                            <option value="Male">Male</option>
                            <option value="female">feMale</option>

                        </select>
                    </div>
                  </div>



                  <div class="col-md-3">
                    <div class="form-group">
                        <label for="status">status</label>

                        <select class="form-select form-select-sm" name="status" id="status" style="width: 100%;"
                         aria-label=".form-select-sm example">

                            <option selected>{{$change->user->status}}</option>
                            <option value="Active">Active</option>
                            <option value="InActive">InActive</option>

                        </select>
                    </div>
                  </div>











                </div>




              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="button" onclick="preformUpdate({{$change->id}})" class="btn btn-primary">update</button>
              </div>
            </form>
          </div>



        </div>



      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->



@endsection


@section('script')
<script>
    function preformUpdate(id){
        let data ={
            firstName: document.getElementById('firstName').value,
            lastName: document.getElementById('lastName').value,
            mobile: document.getElementById('mobile').value,
            email: document.getElementById('email').value,
            // password: document.getElementById('password').value,
            gender: document.getElementById('gender').value,
            status: document.getElementById('status').value,
        }

        let redirectUrl = '/cms/admin/change'

        update('/cms/admin/change/'+id , data, redirectUrl);
    }
</script>

@endsection
